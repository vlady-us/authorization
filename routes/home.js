var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.isAuthenticated()){
    res.redirect('/inner');
  }else {
    res.render('index', {message: req.flash('message')});
  }
});

module.exports = router;
