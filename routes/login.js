var express = require('express');
var router = express.Router();
var controller = require('../controllers')

/* POST login page. */
router.post('/login', controller.login);

module.exports = router;
