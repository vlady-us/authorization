const express = require('express');
const router = express.Router();
const indexRouter = require('./home');
const innerRouter = require('./inner');
const loginRouter = require('./login');

const isAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) return next();
  res.redirect('/')
};

module.exports = function(passport){

  router.get('/', indexRouter);

  router.post('/login', passport.authenticate('login', {
    successRedirect: '/inner',
    failureRedirect: '/',
    failureFlash : true
  }));

  router.get('/inner', isAuthenticated, innerRouter);

  router.get('/logout', function (req,res){
    req.logout();
    res.redirect('/')
  })

  router.get('/error', function (req,res){
    res.render('error', {
      message: "not found",
      error:{
        status: 404,
        stack: 'error happened'
      }
    });
  });

  router.get('*', function(req, res){
    res.status(404).send('ERROR 404');
  });

  return router;
};
