const loginController = require('./loginController')
const innerController = require('./innerController')
module.exports = {
  login: loginController,
  inner: innerController
}
