const innerController = function (req, res) {
  res.render('inner', { user: req.user || 'user' });
};


module.exports = innerController;
