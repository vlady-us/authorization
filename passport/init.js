var login = require('./login');
var User = require('../models/user');

module.exports = function(passport){

  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  // Setting up Passport Strategies for Login and SignUp/Registration
  login(passport);
};
