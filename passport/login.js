var LocalStrategy   = require('passport-local').Strategy;
var User = require('../models/user');

module.exports = function(passport){

  passport.use('login', new LocalStrategy({
        passReqToCallback : true
      },
      function(req, username, password, done) {
        if(username === 'user' &&  password === 'user') return done(null, true);
        return done(null, false, req.flash('message', 'User Not found.'));
      }
  ));

}
